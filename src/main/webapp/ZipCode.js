
function updateCity(){
    if(this.value.length !=4) return;
    const xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.onload = onLoad;
    //xmlHttpRequest.open("GET","https://www.noelvaes.eu/Ajax/zipcode?code="+this.value,true);
    xmlHttpRequest.open("GET","http://localhost:8080/zipcodes_ajax/zipcode?code="+this.value,true);
    xmlHttpRequest.send(null);
}

function onLoad(){
    const city = document.getElementById("cityField");
    city.options.length=0;
    const cities = this.responseText.split(";",100);
    for(let i=0; i<cities.length-1;i++){
        const option = document.createElement('option');
        option.text = cities[i];
        city.options[i] = option;
    }
}

function init(){
    const zipCode = document.getElementById("zipCodeField");
    zipCode.addEventListener("keyup",updateCity)
}

window.addEventListener("load",init);