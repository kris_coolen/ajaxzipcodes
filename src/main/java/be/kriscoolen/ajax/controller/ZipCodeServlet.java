package be.kriscoolen.ajax.controller;

import be.kriscoolen.ajax.domain.ZipCode;
import be.kriscoolen.ajax.repository.ZipCodeDao;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet(value="/zipcode")
public class ZipCodeServlet extends HttpServlet {
    @Resource(name="ZipCodeDS")
    private DataSource dataSource;
    private ZipCodeDao dao;

    @Override
    public void init(){
        dao = new ZipCodeDao();
        dao.setDataSource(dataSource);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String code = req.getParameter("code");
        try(PrintWriter out = resp.getWriter()){
            resp.setContentType("text/plain");
            resp.addHeader("Access-Control-Allow-Origin","*");
            if(code!=null){
                int c = Integer.parseInt(code);
                List<ZipCode> zipCodes = dao.getZipCodesByCode(c);
                for(ZipCode zipCode: zipCodes){
                    out.println(zipCode.getName()+";");
                }
            }
        }catch (SQLException e){
            throw new ServletException("Exception",e);
        }
    }
}
