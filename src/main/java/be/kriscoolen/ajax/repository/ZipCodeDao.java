package be.kriscoolen.ajax.repository;

import be.kriscoolen.ajax.domain.ZipCode;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ZipCodeDao {

    private final static String QUERY_BY_CODE =
            "SELECT id,code,name FROM ZipCodes WHERE code = ? ORDER BY name";

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<ZipCode> getZipCodesByCode(int code) throws SQLException{
        try(Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement(QUERY_BY_CODE))
        {
            stmt.setInt(1,code);
            try(ResultSet rs = stmt.executeQuery()){
                List<ZipCode> zipCodes = new ArrayList<>();
                while(rs.next()){
                    zipCodes.add
                            (
                            new ZipCode(rs.getInt(1),
                                        rs.getInt(2),
                                        rs.getString(3)
                                       )
                            );
                }
                return zipCodes;
            }
        }
    }

    public Connection getConnection() throws SQLException{
        return dataSource.getConnection();
    }
}
